$(function () {
  $('.services-list a').on('click', function (e) {
    e.preventDefault();
    let id = $(this).attr('href');

    $('.sirvices-description').each(function (el) {
      el = $(this).attr('id');
      if (id === el) {
        $(this).removeClass('hidden-el');
      } else {
        $(this).addClass('hidden-el');
      }
    });
  });

  //  SLIDER

  $('.reviews-slider-list').on('click', function (e) {
    e.preventDefault();
    $('.reviews-slider-list').each(() => {
      $('.reviews-slider-list').removeClass('hover-el');
    });
    $(this).addClass('hover-el');
    addBlock();
  });

  $('.reviews-button-next').on('click', function () {
    if ($('a.hover-el').index() === $('a.reviews-slider-list').last().index()) {
      $('a.hover-el').removeClass('hover-el');
      $('a.reviews-slider-list').first().addClass('hover-el');
    } else {
      $('a.hover-el').next('a').addClass('hover-el');
      $('a.hover-el').prev('a').removeClass('hover-el');
    }
    addBlock();
  });

  $('.reviews-button-previous').on('click', function () {
    if ($('a.hover-el').index() === 0) {
      $('a.hover-el').removeClass('hover-el');
      $('a.reviews-slider-list').last().addClass('hover-el');
    } else {
      $('a.hover-el').prev('a').addClass('hover-el');
      $('a.hover-el').next('a').removeClass('hover-el');
    }
    addBlock();
  });

  function addBlock() {
    $('.reviews-description').each(function addBlock(index) {
      if ($('a.hover-el').index() === index) {
        $(this).removeClass('hide-el');
      } else {
        $(this).addClass('hide-el');
      }
    });
  }

  //   Work

  $('.work-list').on('click', 'li', function (e) {
    e.preventDefault();
    $('.work-list li').removeClass('focus-el');
    $(this).addClass('focus-el');
    let elAttr = $(this).attr('data-name');
    $('.work-logo li').each(function () {
      $(this).hide();
      if ($(this).attr('data-name') === elAttr) {
        $(this).show();
      } else if (elAttr === 'work-item-all') {
        $(this).show();
      }
    });
  });

  $('.work-button').on('mousedown', function () {
    return false;
  });

  $('.work-button').on('click', function (e) {
    e.preventDefault();
    let arrayTekenen = [
      './img/landingpage/wordpress4.jpg',
      './img/landingpage/wordpress1.jpg',
      './img/landingpage/webdesign6.jpg',
      './img/landingpage/landingpage7.jpg',
      './img/landingpage/landingpage5.jpg',
      './img/landingpage/landingpage2.jpg',
      './img/landingpage/graphicdesign11.jpg',
      './img/landingpage/graphicdesign9.jpg',
      './img/landingpage/graphicdesign3.jpg',
      './img/landingpage/graphicdesign2.jpg',
      './img/landingpage/landingpage4.jpg',
      './img/landingpage/graphicdesign8.jpg',
    ];
    $.each(arrayTekenen, function () {
      let cloneEl = $('.work-logo-item:first').clone(true);
      if (this.indexOf('wordpress') > -1) {
        cloneEl.attr('data-name', 'work-item-wpress');
      } else if (this.indexOf('webdesign') > -1) {
        cloneEl.attr('data-name', 'work-item-web');
      } else if (this.indexOf('landingpage') > -1) {
        cloneEl.attr('data-name', 'work-item-landing');
      } else if (this.indexOf('graphicdesign') > -1) {
        cloneEl.attr('data-name', 'work-item-graphic');
      }
      cloneEl.appendTo($('.work-logo'));
      $('.work-logo-item:last img').attr('src', this);
    });
    $('.work-logo li').each(function () {
      $(this).hide();
      if (
        $(this).attr('data-name') ===
        $('.work-list li.focus-el').attr('data-name')
      ) {
        $(this).show();
      } else if (
        $('.work-list li.focus-el').attr('data-name') === 'work-item-all'
      ) {
        $(this).show();
      }
    });
    $(this).remove();
  });
});
