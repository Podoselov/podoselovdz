'use strict';

const createEl = function (arr, parent = body) {
  let ul = document.createElement('ul');
  let newArr = arr.map((item) => {
    let li = `<li>${item}</li>`;
    return li;
  });
  for (const iterator of newArr) {
    ul.insertAdjacentHTML('beforeend', iterator);
  }
  parent.prepend(ul);
};
let body = document.body;
let div = document.querySelector('.list');
createEl(['1', '2', '3', 'sea', 'user', 23], div);
