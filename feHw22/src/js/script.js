$(function () {
  'use strict';
  $('.burger').on('click', function (e) {
    $('.burger img').each(function () {
      $(this).toggleClass('burger__hidden-el');
    });
    $('.menu--opened').toggleClass('burger__hidden-el');
  });
});
