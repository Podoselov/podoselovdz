'use strict';

const btnEl = document.querySelector('.link-change');
const ulEl = document.querySelector('.header-list');
const footerEl = document.querySelector('.footer-info');

window.onload = function load() {
  if (localStorage.getItem('style') === 'add-style') {
    ulEl.classList.add('change-style');
    footerEl.classList.add('change-style');
  }
};

function changeStyle() {
  ulEl.classList.toggle('change-style');
  footerEl.classList.toggle('change-style');
  if (
    ulEl.classList.contains('change-style') &&
    footerEl.classList.contains('change-style')
  ) {
    localStorage.setItem('style', 'add-style');
  } else {
    localStorage.setItem('style', 'defolt');
  }
}

btnEl.addEventListener('click', changeStyle);
