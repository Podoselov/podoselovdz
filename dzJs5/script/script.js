'use strict';

function createNewUser() {
  let userName = prompt('Your name');
  while (userName === null || userName.trim() === '');
  let userLastName = prompt('Your last name');
  while (userLastName === null || userLastName.trim() === '');
  let userBirthDay = prompt('Your birthday', 'dd.mm.yyyy');
  while (userBirthDay === null || userBirthDay.trim() === '');

  const newUser = {
    firstName: userName,
    lastName: userLastName,
    birthday: userBirthDay,
    getPassword: function () {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6)
      );
    },
    getAge: function () {
      let date = this.birthday.split('.');
      let dateNow = new Date();
      let userDate = new Date(date[2], date[1] - 1, date[0]);
      let age;
      if (
        userDate.getMonth() > dateNow.getMonth() ||
        (userDate.getMonth() == dateNow.getMonth() &&
          userDate.getDate() > dateNow.getDate())
      ) {
        age = dateNow.getFullYear() - userDate.getFullYear() - 1;
      } else {
        age = dateNow.getFullYear() - userDate.getFullYear();
      }
      return age;
    },
  };
  return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getPassword());
console.log(user.getAge());
