'use strict';

let userNumber = prompt('Write the number');

while (
  userNumber === null ||
  typeof +userNumber !== 'number' ||
  isNaN(+userNumber) ||
  userNumber.trim() === ''
) {
  userNumber = prompt('Write the number');
}

if (userNumber > 4) {
  for (let index = 0; index < userNumber; index += 5) {
    console.log(index);
  }
} else {
  console.log(`Sorry, no numbers`);
}
