'use strict';

const iEl = document.querySelectorAll('.fas');
const btnEl = document.querySelector('.btn');

function clickhandler(e) {
  if (e.target.tagName === 'I') {
    if (e.target.previousElementSibling.type === 'password') {
      e.target.classList.add('fa-eye-slash');
      e.target.classList.remove('fa-eye');
      e.target.previousElementSibling.type = 'text';
    } else {
      e.target.classList.remove('fa-eye-slash');
      e.target.classList.add('fa-eye');
      e.target.previousElementSibling.type = 'password';
    }
  }

  if (e.target.tagName === 'BUTTON') {
    const message = document.querySelector('.message');
    const inp1 = document.getElementById('input1');
    const inp2 = document.getElementById('input2');
    if (inp1.value === inp2.value && inp1.value !== '' && inp2.value !== '') {
      message.classList.add('hidden-el');
      alert('You are welcome');
      inp1.value = '';
      inp2.value = '';
      inp1.nextElementSibling.classList.remove('fa-eye-slash');
      inp1.nextElementSibling.classList.add('fa-eye');
      inp1.type = 'password';
      inp2.nextElementSibling.classList.remove('fa-eye-slash');
      inp2.nextElementSibling.classList.add('fa-eye');
      inp2.type = 'password';
    } else {
      message.classList.remove('hidden-el');
    }
    e.preventDefault();
  }
}
Array.from(iEl).forEach((el) => {
  el.addEventListener('click', clickhandler);
});
btnEl.addEventListener('click', clickhandler);
