'use strict';

const inp = document.querySelector('.input');
const div = document.querySelector('.container');
const divHidden = document.querySelector('.hidden-el');
const spanFocus = document.querySelector('.hidden-span');
const btn = document.querySelector('.reset-button');
const p = document.querySelector('p');

inp.addEventListener('focusin', onInputFocus);
function onInputFocus() {
  inp.style.outline = 'green';
}

inp.addEventListener('focusout', onInputFocusOut);
function onInputFocusOut() {
  if (inp.value < 0) {
    inp.style.border = '1px solid red';
    p.style.display = 'block';
    inp.style.color = 'black';
    divHidden.style.display = 'none';
    inp.value = '';
  } else {
    inp.style.border = '1px solid black';
    inp.style.color = 'lime';
    divHidden.style.display = 'block';
    p.style.display = 'none';
    spanFocus.innerText = `Текущая цена: ${inp.value}`;
  }
}

btn.addEventListener('click', onBtnClick);
function onBtnClick() {
  divHidden.style.display = 'none';
  inp.value = '';
}
