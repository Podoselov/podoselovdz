'use strict';

const imgEl = document.querySelectorAll('.image-to-show');
let focusEl = 1;

const stopBtn = document.getElementById('stop');
const startBtn = document.getElementById('start');

let timer = setInterval(showElement, 3000);

function showElement() {
  imgEl.forEach((el, index) => {
    el.classList.remove('show-el');
    el.classList.add('hidden-el');
    if (index === focusEl) {
      el.classList.remove('hidden-el');
      el.classList.add('show-el');
    }
  });
  focusEl++;
  if (focusEl >= imgEl.length) {
    focusEl = 0;
  }
}

function stopTimer() {
  clearInterval(timer);
}

function startTimer() {
  clearInterval(timer);
  timer = setInterval(showElement, 3000);
}

stopBtn.addEventListener('click', stopTimer);
startBtn.addEventListener('click', startTimer);
