'use strict';

const filterBy = function (arr, type) {
  return arr.filter((item) => typeof item !== type);
};

console.log(filterBy(['aaa', 2, null, 'sss', true, {}], 'string'));
