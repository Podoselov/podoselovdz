'use strict';

const ulNav = document.querySelector('.tabs');
const liNav = document.querySelectorAll('.tabs-title');
const liNavArr = Array.from(liNav);
const liText = document.querySelectorAll('.tabs-content');

function clickhandler(e) {
  if (e.target.tagName == 'LI') {
    liNavArr.forEach((el) => {
      el.classList.remove('active');
    });
    e.target.closest('li').classList.add('active');
    liText.forEach((el) => {
      el.classList.add('hidden');
      if (liNavArr.indexOf(e.target) === +el.getAttribute('data-index')) {
        el.classList.remove('hidden');
      }
    });
  }
}

ulNav.addEventListener('click', clickhandler);
