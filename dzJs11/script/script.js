'use strict';

const btnEl = document.querySelectorAll('.btn');

function keyUpBtn(e) {
  btnEl.forEach((el) => {
    el.classList.remove('active');
    if (el.textContent.toLowerCase() === e.key.toLowerCase())
      el.classList.add('active');
  });
}
document.addEventListener('keyup', keyUpBtn);
