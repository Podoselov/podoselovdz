'use strict';

function createNewUser() {
  let userName;
  let userLastName;
  do {
    userName = prompt('Your name');
  } while (userName === null || userName.trim() === '');
  do {
    userLastName = prompt('Your last name');
  } while (userLastName === null || userLastName.trim() === '');

  const newUser = {
    firstName: userName,
    lastName: userLastName,
    getLogin: function () {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  };
  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
