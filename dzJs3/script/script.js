'use strict';

let num1;
let num2;
let operator;

do {
  num1 = prompt('First number');
} while (
  num1 === null ||
  typeof +num1 !== 'number' ||
  isNaN(+num1) ||
  num1.trim() === ''
);

do {
  num2 = prompt('Second number');
} while (
  num2 === null ||
  typeof +num2 !== 'number' ||
  isNaN(+num2) ||
  num2.trim() === ''
);
operator = prompt('What operation: + , - , / , * ');

function calc(firstItem, secondItem, operation) {
  let res;
  switch (operation) {
    case '+':
      res = +firstItem + +secondItem;
      break;
    case '-':
      res = firstItem - secondItem;
      break;
    case '/':
      res = firstItem / secondItem;
      break;
    case '*':
      res = firstItem * secondItem;
      break;

    default:
      alert('Wrong operator');
      break;
  }
  return res;
}

console.log(calc(num1, num2, operator));
